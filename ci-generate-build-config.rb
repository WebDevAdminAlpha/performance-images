#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'http'
require 'json'
require 'optimist'
require 'pathname'
require 'rainbow'
require 'semantic'
require 'yaml'

ci_dir = Pathname.new(File.expand_path('.gitlab/ci', __dir__)).relative_path_from(Dir.pwd)

@opts = Optimist.options do
  banner "Usage: ci-generate-build-config [options]"
  banner "\nGenerates config for GitLab Performance Test Comparision pipeline against custom docker images"
  banner "\nOptions:"
  opt :help, 'Show help message'
  opt :target_version, 'Latest GitLab version to target. Defaults to latest release version', type: :string
  opt :target_number, 'Number of previous GitLab docker versions to build against target', type: :int, default: 5
  opt :min_base_version, 'Minimum Base version available that GitLab docker images can be built with. This shouldn\'t be changed unless specifically required.', type: :string, default: '12.5.0'
  opt :extends, 'CI config key to extend from in generated jobs', type: :string, default: '.gpt-build-images-base'
  opt :conf_file, 'Path where generated CI config file will be saved', type: :string, default: "#{ci_dir}/gpt-build-images-jobs.yml"
  banner "\nEnvironment Variable(s):"
  banner "  CI_SLACK_REPORT             Set jobs to report results to Slack (Default: nil)"
  banner "  CI_WIKI_REPORT              Set jobs to report results to Wiki (Default: nil)"
end

def get(url, headers = {})
  HTTP.follow.get(
    url,
    headers: headers
  )
end

def get_latest_gitlab_version(search_str: nil)
  url = "https://gitlab.com/api/v4/projects/13083/repository/tags" + (search_str ? "?search=#{search_str}" : '')
  latest_vers_list = JSON.parse(get(url).body.to_s).reject { |item| item['name'].match(/pre|rc/) }
  raise "No GitLab versions found online" + (" with search string '#{search_str}'" if search_str) + '. ' + 'Exiting...' if latest_vers_list.empty?

  latest_major_minor_ver = latest_vers_list.map { |ver| Semantic::Version.new(ver['name'].match(/v(\d+\.\d+\.\d+)/)[1]) }.uniq.max
  latest_major_minor_ver.patch = 0
  latest_major_minor_ver
end

def get_last_versions(target_ver:, min_base_version:, target_num:)
  puts "Getting last #{target_num} versions from #{target_ver}...\n\n"
  last_versions = []

  target_num.times do |prev_ver_count|
    ver_to_add = target_ver.dup
    if ver_to_add.minor - prev_ver_count >= 0
      ver_to_add.minor -= prev_ver_count
      raise ArgumentError, Rainbow("Version to build, '#{ver_to_add}', is older than the minimum allowed version of '#{min_base_version}'. All versions to be built must be higher than '#{min_base_version}'. Exiting...").red if ver_to_add < min_base_version

      last_versions.prepend(ver_to_add)
    elsif (ver_to_add.major - 1).positive?
      prev_ver_to_add = get_latest_gitlab_version(search_str: "v#{ver_to_add.major - 1}")
      last_versions += get_last_versions(target_ver: prev_ver_to_add, min_base_version: min_base_version, target_num: target_num - prev_ver_count)
      break
    end
  end

  last_versions.sort
end

target_ver = @opts[:target_version] ? Semantic::Version.new(@opts[:target_version]) : get_latest_gitlab_version
target_num = ENV['GPT_BUILD_TARGET_NUMBER'] ? ENV['GPT_BUILD_TARGET_NUMBER'].to_i : @opts[:target_number]
min_base_version = Semantic::Version.new(ENV['GPT_BUILD_min_base_version'] || @opts[:min_base_version])

last_vers = get_last_versions(target_ver: target_ver, min_base_version: min_base_version, target_num: target_num)
gpt_build_conf = {}

last_vers.each do |ver|
  gpt_build_test_name = "#{ver.major}-#{ver.minor}-build"
  gpt_build_conf[gpt_build_test_name] = {}
  gpt_build_conf[gpt_build_test_name]['extends'] = @opts[:extends] if @opts[:extends]
  gpt_build_conf[gpt_build_test_name]['variables'] = {
    'MAJOR_VERSION' => ver.major,
    'MINOR_VERSION' => ver.minor
  }
end

gpt_build_conf_yaml = gpt_build_conf.to_yaml({ line_width: -1 })
puts "Generated config:\n#{gpt_build_conf_yaml}\n"
File.write(@opts[:conf_file], gpt_build_conf_yaml)
puts "Saved GPT Build Image CI config to #{@opts[:conf_file]}\n"
