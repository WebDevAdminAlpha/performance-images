#!/usr/bin/env ruby

$stdout.sync = true

require 'chronic_duration'
require 'down/http'
require 'fileutils'
require 'http'
require 'json'
require 'open3'
require 'optimist'
require 'rainbow'
require 'semantic'
require 'time'

@opts = Optimist.options do
  banner "Usage: build-gitlab-images [options]"
  banner "\n"
  banner "\nOptions:"
  opt :start_version, "Start version", type: :string, default: '12.5.0'
  opt :end_version, "End version", type: :string
  opt :base_version, "Base version", type: :string, default: '12.5.0'
  opt :edition, "Edition", type: :string, default: "ce"
  opt :base_registry, "Base Registry", type: :string, default: 'registry.gitlab.com/gitlab-org/quality/performance-images/gitlab-ce-performance-base'
  opt :target_registry, "Target Registry", type: :string, default: 'registry.gitlab.com/gitlab-org/quality/performance-images/gitlab-ce-performance'
  opt :push, "Push image after build. Requires Docker Login beforehand", type: :flag, default: false
  opt :delete, "Delete after push", type: :flag, default: false
  opt :force, "Force image builds even if they exist.", type: :flag, default: false
  opt :help, 'Show help message'
end

raise 'Docker install is missing. Docker must be installed before running this script. Exiting...' unless Open3.capture2e('docker', 'version')[1].success?

def get(url, headers = {})
  HTTP.follow.get(
    url,
    headers: headers
  )
end

def run_command(cmd, fail_on_error = true)
  status = nil

  Open3.popen2e(cmd) do |stdin, stdout_stderr, wait_thr|
    stdin.close
    stdout_stderr.each do |line|
      puts line.lstrip
    end
    status = wait_thr.value
  end

  raise "'#{cmd}' failed..." unless status.success?

  status.success?
end

def setup_docker_copyedit
  docker_copyedit_tmp_path = "#{Dir.tmpdir}/docker-copyedit.py"

  ['docker-copyedit.py', docker_copyedit_tmp_path, File.join(Dir.pwd, 'docker-copyedit.py')].each do |docker_copyedit|
    return docker_copyedit if Open3.capture2e("python3 #{docker_copyedit} -h")[0].strip.include?('Usage: docker-copyedit.py')
  end

  docker_copyedit_url = ENV['DOCKER_COPYEDIT_URL'] || "https://raw.githubusercontent.com/gdraheim/docker-copyedit/master/docker-copyedit.py"
  warn Rainbow("docker-copyedit.py not found, downloading from #{docker_copyedit_url}...").yellow
  Down::Http.download(docker_copyedit_url, destination: docker_copyedit_tmp_path)

  docker_copyedit_tmp_path
end

def get_latest_gitlab_version(search_str = nil)
  url = "https://gitlab.com/api/v4/projects/13083/repository/tags" + (search_str ? "?search=#{search_str}" : '')
  latest_vers_list = JSON.parse(get(url).body.to_s).reject { |item| item['name'].match(/pre|rc/) }
  raise "No GitLab versions found online" + (" with search string '#{search_str}'" if search_str) + '. ' + 'Exiting...' if latest_vers_list.empty?

  latest_major_minor_ver = latest_vers_list.map { |ver| Semantic::Version.new(ver['name'].match(/v(\d+\.\d+\.\d+)/)[1]) }.uniq.max
  latest_major_minor_ver.patch = 0
  latest_major_minor_ver
end

def build_versions_dict(start_ver, end_ver)
  versions_dict = {}

  case end_ver.major - start_ver.major
  when 0
    versions_dict[start_ver.major] = { start: start_ver, end: end_ver }
  when 1
    versions_dict[start_ver.major] = { start: start_ver, end: get_latest_gitlab_version("v#{start_ver.major}") }
    versions_dict[end_ver.major] = { start: get_latest_gitlab_version("v#{end_ver.major}.0.0"), end: end_ver }
  else
    versions_dict[start_ver.major] = { start: start_ver, end: get_latest_gitlab_version("v#{start_ver.major}") }
    ((start_ver.major + 1)..(end_ver.major - 1)).each do |between_ver|
      versions_dict[between_ver] = { start: get_latest_gitlab_version("v#{between_ver}.0.0"), end: get_latest_gitlab_version("v#{between_ver}") }
    end
    versions_dict[end_ver.major] = { start: get_latest_gitlab_version("v#{end_ver.major}.0.0"), end: end_ver }
  end

  versions_dict
end

def check_base_image_exists(base_version)
  puts "Checking if base image #{@opts[:base_registry]}:#{base_version} already exists...\n"
  res = Open3.capture2e('docker', 'manifest', 'inspect', "#{@opts[:base_registry]}:#{base_version}")[1].success?
  puts "Base image #{@opts[:base_registry]}:#{base_version} doesn't exist. Skipping...\n\n" unless res
  res
end

def check_target_image_exists(version)
  return false if @opts[:force] || ENV['FORCE_IMAGE_BUILD'] == 'true'

  puts "Checking if image #{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0 already exists...\n"
  res = Open3.capture2e('docker', 'manifest', 'inspect', "#{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0")[1].success?
  puts res ? "Image #{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0 already exists on registry. Skipping...\n\n" : "Image #{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0 doesn't already exist on registry. Building...\n\n"
  res
end

def get_release_image_tag(version)
  puts "Getting release image tag for #{version}"
  url = 'https://registry.hub.docker.com/v1/repositories/gitlab/gitlab-ce/tags'
  tag = JSON.parse(get(url).body.to_s).map { |tag| tag['name'] }.find { |tag| tag.include?("#{version}-#{@opts[:edition]}") }
  puts "Release tag for #{version} is #{tag}"
  tag
end

def build_image(version, base_version)
  return if check_target_image_exists(version) || !check_base_image_exists(base_version)

  puts "Building Docker Image #{version}"
  build_start_time = Time.now.to_i

  release_tag = get_release_image_tag(version)
  puts "\nPulling gitlab/gitlab-#{@opts[:edition]}:#{release_tag}..."
  run_command("docker pull gitlab/gitlab-#{@opts[:edition]}:#{release_tag}")

  if Open3.capture2('docker', 'images', '-q', "gitlab-ce:#{release_tag}")[0].strip == ''
    puts "\nRemoving volumes from gitlab/gitlab-#{@opts[:edition]}:#{release_tag}..."
    docker_copyedit = setup_docker_copyedit
    run_command("python3 #{docker_copyedit} FROM gitlab/gitlab-ce:#{release_tag} INTO no-volumes/gitlab-ce:#{release_tag} REMOVE ALL VOLUMES")
  end

  puts "\nBuilding Performance GitLab image version #{version} from base image #{@opts[:base_registry]}:#{base_version}"
  run_command("docker build --build-arg gitlab_base_registry=#{@opts[:base_registry]} --build-arg gitlab_base_version=#{base_version} --build-arg gitlab_release_tag=#{release_tag} -t #{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0 .")

  if @opts[:push]
    puts "\nPushing #{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0..."
    run_command("docker push #{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0")
    run_command("docker rmi #{@opts[:target_registry]}:#{version}-#{@opts[:edition]}.0") if @opts[:delete]
  end

  puts "\nRemoving base images..."
  run_command("docker image prune -f")
  run_command("docker rmi -f gitlab/gitlab-ce:#{release_tag}")
  run_command("docker rmi -f no-volumes/gitlab-ce:#{release_tag}")

  puts "\nBuild of Docker Image #{version} completed in #{ChronicDuration.output(Time.now.to_i - build_start_time, format: :long)}"
end

def build_major_images(start_version:, end_version:, base_version:)
  version = start_version.dup
  major_base_version = base_version.major == version.major ? base_version : Semantic::Version.new("#{version.major}.0.0")
  while version <= end_version
    build_image(version, major_base_version)
    version.minor += 1
  end
end

puts "GitLab Performance Docker Image Builder\n\n"

start_version = Semantic::Version.new(@opts[:start_version])
end_version = @opts[:end_version] ? Semantic::Version.new(@opts[:end_version]) : get_latest_gitlab_version
base_version = Semantic::Version.new(@opts[:base_version])

raise "End Version ('#{end_version}') is lower than Start Version ('#{start_version}'). Exiting..." if start_version > end_version
raise "Base Version ('#{base_version}') isn't equal to or lower than Start Version ('#{start_version}'). Exiting..." if base_version > start_version

puts "Building images from #{start_version} to #{end_version}\n\n"

start_time = Time.now.to_i
versions_dict = build_versions_dict(start_version, end_version)

versions_dict.each do |_, versions|
  build_major_images(start_version: versions[:start], end_version: versions[:end], base_version: base_version)
end

puts "\nBuild of images completed in #{ChronicDuration.output(Time.now.to_i - start_time, format: :long)}"
